<?php
/**
 * Media Shard Helper
 * @author beeplogic <beeplogic@magenation.com>
 */
class MageNation_MediaShard_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * get number of product urls to shard
     * @return int
     */
    public function getNumShards()
    {
        $key        = $this->_getSecureKey();
        return (int) Mage::getStoreConfig('web/'.$key.'/media_url_shards');
    }
    /**
     * get appropriate key to use for config path
     * based on whether current store is secure or not
     * @return str
     */
    protected function _getSecureKey()
    {
        $isSecure   = Mage::app()->getStore()->isCurrentlySecure();
        $key        = $isSecure ? 'secure' : 'unsecure';
        return $key;
    }
    /**
     * check whether or not media urls should be
     * sharded or not
     * @return bool
     */
    public function shardMediaUrl()
    {
        $key        = $this->_getSecureKey();
        return Mage::getStoreConfigFlag('web/'.$key.'/shard_media_url');
    }
    /**
     * parses a string for the product image URL and replaces it
     * with a "sharded" one
     *
     * @param str $content
     * @return str
     */
    public function shardString($content)
    {
        $mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $pieces   = parse_url($mediaUrl);
        $imageUrl = $pieces['scheme'].'://'.$pieces['host'].'/';
        $newContent = preg_replace_callback("#($imageUrl)([^\s\"'>]*)#", array($this, '_shardStringCallback'), $content);
        return $newContent;
    }
    /**
     * call back handler for preg_replace_callback
     * will take the sum of ascii character values and use remainder of
     * value divided by number of asset servers
     *
     * @param array $matches
     * @return str
     * @see self::shardString
     */
    protected function _shardStringCallback($matches)
    {
        $pathLength = strlen($matches[2]);
        $sum        = 0;
        $numShards  = $this->getNumShards();
        for ($i =0; $i < $pathLength; $i++) {
            $sum += ord($matches[2]{$i});
        }
        $shard = $sum % $numShards;
        if ($shard == 0) {
            $shard = 1;
        }
        $server = preg_replace('|(https?://)([\w]+)|', '${1}${2}'.$shard, $matches[1]);
        return $server.$matches[2];
    }
}