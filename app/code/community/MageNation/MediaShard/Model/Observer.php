<?php
/**
 * 
 * Observer Model
 * @author beeplogic <beeplogic@magenation.com>
 */
class MageNation_MediaShard_Model_Observer
{
	/**
     * parse response body for product media URL and replace
     * @param Varien_Event_Observer $observer
     */
    public function responseSendBefore(Varien_Event_Observer $observer)
    {
        /* @var $helper MageNation_MediaShard_Helper_Data */
        $helper   = Mage::helper('mn_mediashard');
        if ($helper->shardMediaUrl()) {
            /* @var $response Mage_Core_Controller_Response_Http */
            $response = $observer->getResponse();
            $body     = $response->getBody(true);
            foreach ($body as $part => $content) {
                $response->setBody($helper->shardString($content), $part);
            }
        }
    }
}